import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
const KJUR = require('jsrsasign');
import Router from "next/router";
import Head from "next/head";
import { groupServices } from '../../../store/services';
import { getFromCookies } from "../../../lib/utils";
import { APPCONST } from "../../../constants/globalVar";
import { logout } from '../../../lib/webAuth';

const ZoomMettings = ({ groupId, meetingId, zak, pwd }) => {

  let dispatch = useDispatch();
  const userData = useSelector((state) => state?.userData?.detail);
  const [mettingSuccess, setMettingSuccess] = useState(false);
  const [zoomKey, setZoomKey] = useState({ sdkKey: '', sdkSecret: '' });
  const [zoomMtg, setZoomMtg] = useState(null);

  function generateSignature(sdkKey, sdkSecret, meetingNumber, role) {

    const iat = Math.round((new Date().getTime() - 30000) / 1000)
    const exp = iat + 60 * 60 * 2
    const oHeader = { alg: 'HS256', typ: 'JWT' }

    const oPayload = {
      sdkKey: sdkKey,
      mn: meetingNumber,
      role: role, //ned to integration 
      iat: iat,
      exp: exp,
      appKey: sdkKey,
      tokenExp: iat + 60 * 60 * 2
    }

    const sHeader = JSON.stringify(oHeader)
    const sPayload = JSON.stringify(oPayload)
    const sdkJWT = KJUR.jws.JWS.sign('HS256', sHeader, sPayload, sdkSecret);

    return sdkJWT
  }


  const zoomCallInit = () => {
    try {
      zoomMtg.init({
        debug: true,
        isSupportAV: true,
        leaveUrl: '/zoom-disconnect',//URL of page to render whwn meeting is left
        success: (success) => {
          const sdkKey = zoomKey.sdkKey;
          const sdkSecret = zoomKey.sdkSecret;
          const meetingNumber = meetingId; //meeting number 
          const role = !zak ? 0 : 1; //1 for host , 0 for user
          //  const role=0;
          const signature = generateSignature(sdkKey, sdkSecret, meetingNumber, role)
          // ZoomMtg.join() will go here

          let zoomJoinObj = {
            sdkKey: sdkKey,
            signature: signature,
            meetingNumber: meetingNumber,
            passWord: pwd,  //meeting password
            userName: userData?.email, //username joiner
            success: (success) => {
              setMettingSuccess(true);
            },
            error: (error) => {
              console.log('error show', error)
            }
          }

          if (zak && zak.length) {
            zoomJoinObj.zak = zak;
          }

          zoomMtg.join(zoomJoinObj)
        },
        error: (error) => {
          console.log('errorsss ', error)
        }
      })

    } catch (err) {
      console.error(' errror 105', err)
    }
  }

  const getZoomKeyFromLeader = async () => {
    try {
      const accessToken = getFromCookies(APPCONST.LOGINAUTHTOKENNAME);
      const response = await groupServices.getZoomKeyFromLeader(accessToken, groupId);

      if (response.statusCode == 1) {
        let __data = response.responseData?.data;
        setZoomKey({ sdkKey: __data?.sdkKey, sdkSecret: __data?.sdkSecret });
      }

    } catch (e) {
      console.error('error', e);
    }
  }


  useEffect(() => {
    if (!userData?._id) {
      window.location.href = '/';
    }
  }, []);

  useEffect(() => {
    (async () => {
      if (typeof window !== "undefined") {
        const { ZoomMtg } = (await import('@zoomus/websdk'))
        setZoomMtg(ZoomMtg);
        console.log('########33',ZoomMtg);
      }
    })()
  }, [])


  useEffect(() => {
    getZoomKeyFromLeader();
    if (zoomMtg) {
      setTimeout(() => {
        zoomMtg.setZoomJSLib(`${APPCONST.ZOOM_SOURCE_URL}/lib`, '/av')
        zoomMtg.preLoadWasm();
        zoomMtg.prepareWebSDK();
        zoomMtg.prepareJssdk();
        zoomCallInit();
      }, 3000);
    }
  }, [zoomMtg]);


  //============== Options list ==========

  //audio
  const audioOptionsFn = () => {
    let elmnt = document.getElementsByClassName("audio-option-menu");
    if (elmnt) {
        elmnt[0].classList.toggle("open");
    }
  }

  //video 
  const vidoOptionsFn= ()=>{
    let elmnt = document.getElementsByClassName("video-option-menu");
    if (elmnt) {
        elmnt[0].classList.toggle("open");
    }
  }


    //share screen 
    const shareOptionsFn= ()=>{
      let elmnt = document.getElementsByClassName("sharing-setting-dropdown-menu-container");
      if (elmnt) {
          elmnt[0].classList.toggle("open");
      }
    }

    //share chat

    const chatOptionsFn= ()=>{
      let elmnt = document.getElementsByClassName("footer-chat-button__settings-container");
      if (elmnt) {
          elmnt[0].classList.toggle("open");
      }
    }
 



  return (
    <>
      <Head>
        <link type="text/css" rel="stylesheet" href={`${APPCONST.ZOOM_SOURCE_URL}/css/bootstrap.css`} />
        <link type="text/css" rel="stylesheet" href={`${APPCONST.ZOOM_SOURCE_URL}/css/react-select.css`} />
      </Head>

      <div className="main-pannel main_profile">
        <div className="gp_pannel">

        </div>
      </div>

      {mettingSuccess ?
        <>
          <div className='zoom_meeting_main'>
            <div className='zoom_call_btn zoom_audio' onClick={() => audioOptionsFn()}></div>
            <div className='zoom_call_btn zoom_video' onClick={() => vidoOptionsFn()}></div>
            <div className='zoom_call_btn zoom_chat' onClick={() => chatOptionsFn()}></div>
            <div className='zoom_call_btn zoom_share' onClick={() => shareOptionsFn()}></div>

            <div className='zoom_call_btn zoom_leave' onClick={() => zoomMtg.leaveMeeting(() => null)}>Leave</div>
            {zak ?
              <div className='zoom_call_btn zoom_end' onClick={() => zoomMtg.endMeeting(() => null)}>End</div>
              : ''}
          </div>

        </>
        : ''
      }
    </>
  );
};

export default ZoomMettings;

export async function getServerSideProps(context) {
  try {
    return {
      props: {
        groupId: context?.query?.groupId ? context?.query?.groupId : null,
        meetingId: context?.query?._id[0]?.trim(),
        zak: context?.query?.zak ? context?.query?.zak : null,
        pwd: context?.query?.pwd ? context?.query?.pwd : null,
      }, // will be passed to the page component as props
    };
  } catch (error) {
    console.log(error);
    return {
      props: {
        groupId: null,
        meetingId: null,
        zak: null,
        pwd: null
      }, // will be passed to the page component as props
    };
  }
}







/* //=========================================

http://localhost:3009/zoom/64782ea993110c5f050f09da/85289442087?zak=eyJ0eXAiOiJKV1QiLCJzdiI6IjAwMDAwMSIsInptX3NrbSI6InptX28ybSIsImFsZyI6IkhTMjU2In0.eyJhdWQiOiJjbGllbnRzbSIsInVpZCI6Ik5GeGpPRnJYUzJtR3NZdkdOOUxwRUEiLCJpc3MiOiJ3ZWIiLCJzayI6IjAiLCJzdHkiOjEsIndjZCI6InVzMDUiLCJjbHQiOjAsIm1udW0iOiI4NTI4OTQ0MjA4NyIsImV4cCI6MTY4NzI1NzU5NiwiaWF0IjoxNjg3MjUwMzk2LCJhaWQiOiItNkl5aHVlWFNOS2RpVHRTZzN5eHVRIiwiY2lkIjoiIn0.kQCihIFNJgRXi8CxoyGbmrgoUZlCMy1t_6Km2sdi9P8&pwd=SEdmTlQwK2VQaGpRZnlqT3dlOGVTZz09 */