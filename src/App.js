// import './App.css';
// import ZoomMtgEmbedded from "@zoomus/websdk/embedded"
import { ZoomMtg } from '@zoomus/websdk'

ZoomMtg.setZoomJSLib('https://source.zoom.us/2.13.0/lib', '/av')
ZoomMtg.preLoadWasm()
ZoomMtg.prepareWebSDK()

const KJUR = require('jsrsasign')

function generateSignature(sdkKey, sdkSecret, meetingNumber, role) {

  const iat = Math.round((new Date().getTime() - 30000) / 1000)
  const exp = iat + 60 * 60 * 2
  const oHeader = { alg: 'HS256', typ: 'JWT' }

  const oPayload = {
    sdkKey: sdkKey,
    mn: meetingNumber,
    role: role,
    iat: iat,
    exp: exp,
    appKey: sdkKey,
    tokenExp: iat + 60 * 60 * 2
  }

  const sHeader = JSON.stringify(oHeader)
  const sPayload = JSON.stringify(oPayload)
  const sdkJWT = KJUR.jws.JWS.sign('HS256', sHeader, sPayload, sdkSecret)
  return sdkJWT
}
function App() {

  ZoomMtg.init({
    leaveUrl: 'http://localhost:3000',//URL of page to render whwn meeting is left
    success: (success) => {
      console.log(success)

      const sdkKey = '';
      const sdkSecret = '';
      const meetingNumber = '' //meeting number 
      const role = 0 //1 for host , 0 for user

      const signature = generateSignature(sdkKey, sdkSecret, meetingNumber, role)
      // ZoomMtg.join() will go here
      ZoomMtg.join({
        sdkKey: sdkKey,
        signature: signature,
        meetingNumber: meetingNumber,
        passWord: '',  //meeting password
        userName: '', //username
        // zak: zakToken, // the host's zak token
        success: (success) => {
          console.log(success)
        },
        error: (error) => {
          console.log(error)
        }
      })
    },
    error: (error) => {
      console.log(error)
    }
  })
  return (
    <>
      <div id="meetingSDKElement">

      </div>
    </>
  );
}

export default App;
